const mysql = require('mysql');

const db = mysql.createConnection ({
    host: 'localhost',
    user: 'root',
    password: '123456',
    database: 'tactech'
});

db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database.');
});

global.db = db;

module.exports = db;

/*
DB INFO
    Tables
        - character(id: int pk, name: varchar, gender: varchar, house: varchar, slug: varchar, rank: int, image_url: varchar)
        - book_character(id: int pk, book_name: varchar, character_id: int fk)
        - title_character(id: int pk, title_name: varchar, character_id: int fk)
*/