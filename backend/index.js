const express = require("express");
const connection = require('./db/connection');
const app = express();

const {getAndSaveAllCharacters} = require('./services/Characters');
const {getCharacters} = require('./services/Characters');
const {getCharacter} = require('./services/Characters');


app.get('/saveCharacters', getAndSaveAllCharacters);
app.get('/characters', getCharacters);
app.get('/characters/:id', getCharacter);

app.use(function(req, res, next) {
    let respuesta = {
        error: true, 
        codigo: 404, 
        mensaje: 'URL no encontrada'
    };
    res.status(404).send(respuesta);
});

//Iniciar servidor con comando: node index.js

app.listen(3001, () => {
    console.log("Server is running on port 3001.");
});

/*
DB INFO
    Tables
        - character(id: int pk, name: varchar, gender: varchar, house: varchar, slug: varchar, rank: int, image_url: varchar)
        - book_character(id: int pk, book_name: varchar, character_id: int fk)
        - title_character(id: int pk, title_name: varchar, character_id: int fk)
*/