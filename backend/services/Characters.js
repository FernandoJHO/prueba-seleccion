const axios = require("axios");

function executeQuery(sql){
    return new Promise(function(resolve, reject) {

        db.query(sql, (err, result) => {
            if (err) {
                return reject(err);
            }

            resolve(result);
        });

    });
}

module.exports = {

    getAndSaveAllCharacters: async (req, res) => {

        let characters;
        let queryCharacter;
        let queryTitle;
        let queryBook;
        let characterId;

        await axios.get('https://api.got.show/api/book/characters').then(result => {
            characters = result.data;
        });

        for (let character of characters){
            let name = character.name || null;
            let gender = character.gender || null;
            let house = character.house || null;
            let slug = character.slug || null;
            let rank = character.pagerank ? character.pagerank.rank : null;
            let image = character.image || null;
            let titles = character.titles;
            let books = character.books;


            queryCharacter = `INSERT INTO characters (name, gender, house, slug, rank, image_url) VALUES ("${name}", "${gender}", "${house}", "${slug}", ${rank}, "${image}")`;
            
            await executeQuery(queryCharacter).then(res => {
                characterId = res.insertId;
                console.log("Id insertado: "+characterId);
            });

            if ( titles.length ){
                for (let title of titles){
                    queryTitle = `INSERT INTO title_character (title_name, character_id) VALUES ("${title}", ${characterId})`;

                    await executeQuery(queryTitle);
                }
            }

            if ( books.length ){

                for (let book of books){
                    queryBook = `INSERT INTO book_character (book_name, character_id) VALUES ("${book}", ${characterId})`;

                    await executeQuery(queryBook);
                }
            }
        }

        res.send("Personajes guardados.");

    },

    getCharacters: async (req, res) => {

        let query = "SELECT id, name, gender, house FROM characters";
        let result;

        await executeQuery(query).then(res => {
            result = res;
        });

        res.send(result);

    },

    getCharacter: async (req, res) => {

        let id = Number(req.params.id);
        let character;
        let books;
        let titles;

        let queryCharacter = `SELECT * FROM characters WHERE id=${id}`;
        let queryBooks = `SELECT book_name FROM book_character WHERE character_id=${id}`;
        let queryTitles = `SELECT title_name FROM title_character WHERE character_id=${id}`;

        await executeQuery(queryCharacter).then(res => {
            character = res[0];
        });

        await executeQuery(queryBooks).then(res => {
            books = res;
        });

        await executeQuery(queryTitles).then(res => {
            titles = res;
        });

        character["books"] = books;
        character["titles"] = titles;

        res.send(character);

    },

};