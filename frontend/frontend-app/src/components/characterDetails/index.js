import React, { Component } from 'react';
import Page from './page';
import ApiClient from '../../utils/ApiClient';

class CharacterDetails extends Component {

    constructor(props){
        super(props);

        this.state = {
            characterId: this.props.match.params.id,
            character: {name: null, house: null, gender: null, slug: null, rank: null, img_url: null, books: [], titles: []}
        };
    }

    async componentDidMount(){
        let idToGet = this.state.characterId;
        await ApiClient.get("/characters/"+idToGet).then(res =>{
            let name = (res.data.name !== "null") ? res.data.name : null;
            let house = (res.data.house !== "null") ? res.data.house : null;
            let gender = (res.data.gender !== "null") ? res.data.gender : null;
            let slug = (res.data.slug !== "null") ? res.data.slug : null;
            let rank = (res.data.rank) ? res.data.rank : null;
            let img_url = (res.data.image_url !== "null") ? res.data.image_url : null;
            let books = [];
            let titles = [];

            if( res.data.books.length ){
                for (let book of res.data.books){
                    books.push(book.book_name);
                }
            }

            if( res.data.titles.length ){
                for (let title of res.data.titles){
                    titles.push(title.title_name);
                }
            }

            this.setState({character: {name, house, gender, slug, rank, img_url, books, titles}});
        });

        document.title = this.state.character.name;
    }

    render(){

        const { character } = this.state;

        return (
            <Page character={character}/>
        );
    }

}

export default CharacterDetails;