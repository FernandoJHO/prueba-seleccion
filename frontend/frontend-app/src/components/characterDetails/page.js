import React from 'react';
import {
    Container,
    Row,
    Col
  } from 'reactstrap';

function Page(props) {

    const {
        character,
    } = props; 

    const books = character.books;
    const titles = character.titles;

    const booksList = books.map(book => <p>{book}</p>);
    const titlesList = titles.map(title => <p>{title}</p>);

    return (
        <Container>
            <br></br>
            <Row>
            <Col> <p>Image: </p> </Col>
                <Col>
                    { (character.img_url) ? <img src={character.img_url} className="img-fluid" alt="Responsive" /> : <p>-------------</p> }
                </Col>
            </Row>
            <br></br>
            <Row>
                <Col> <p>Name: </p> </Col>
                <Col> { (character.name) ? character.name : <p>-------------</p> } </Col>
            </Row>
            <br></br>
            <Row>
                <Col> <p>Gender: </p> </Col>
                <Col> { (character.gender) ? character.gender : <p>-------------</p> } </Col>
            </Row>
            <br></br>
            <Row>
                <Col> <p>Rank: </p> </Col>
                <Col> { (character.rank) ? character.rank : <p>-------------</p> } </Col>
            </Row>
            <br></br>
            <Row>
                <Col> <p>Slug: </p> </Col>
                <Col> { (character.slug) ? character.slug : <p>-------------</p> } </Col>
            </Row>
            <br></br>
            <Row>
                <Col> <p>House: </p> </Col>
                <Col> { (character.house) ? character.house : <p>-------------</p> } </Col>
            </Row>
            <br></br>
            <Row>
                <Col> <p>Books: </p> </Col>
                <Col> { (booksList.length) ? booksList : <p>-------------</p> } </Col>
            </Row>
            <br></br>
            <Row>
                <Col> <p>Titles: </p> </Col>
                <Col> { (titlesList.length) ? titlesList : <p>-------------</p> } </Col>
            </Row>
        </Container>
    );

}

export default Page;