import React, { Component } from 'react';
import Page from './page';
import ApiClient from '../../utils/ApiClient';

class CharactersList extends Component {

    constructor(props){
        super(props);

        this.state = {
            characters: [],
            charactersBackUp: [],
            selectedPage: 1,
            charactersPerPage: 10
        };

        this.handleSelected = this.handleSelected.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    async componentDidMount() {
        document.title = "Characters"

        await ApiClient.get("/characters").then(res => {
            this.setState({characters: res.data, charactersBackUp: res.data});
        });

    }

    handleSelected(selectedPage){
        this.setState({
            selectedPage: Number(selectedPage)
        });
    }

    handleInputChange(event){
        const target = event.target;
        const name = target.name;
        const value = target.value;
        let inputValue = {[name]: value};
        const { characters, charactersBackUp } = this.state;

        const { filterContent } = inputValue;
        let charactersFiltered;
        console.log(filterContent);

        if ( filterContent ){
            charactersFiltered = characters.filter(character => 
                    character.name.toLowerCase().trim().includes(filterContent.toLowerCase().trim()) || character.house.toLowerCase().trim().includes(filterContent.toLowerCase().trim())
                );
            this.setState({characters: charactersFiltered});
        } else{
            this.setState({characters: charactersBackUp});
        }
    }

    render() {

        const { characters, selectedPage, charactersPerPage } = this.state;
        const totalCharacters = characters.length;

        const lastCharacterIndex = selectedPage * charactersPerPage;
        const firstCharacterIndex = lastCharacterIndex - charactersPerPage;
        const currentCharacters = characters.slice(firstCharacterIndex, lastCharacterIndex);

        return (
            <Page currentCharacters={currentCharacters} totalCharacters={totalCharacters} charactersPerPage={charactersPerPage} handleSelected={this.handleSelected} handleInputChange={this.handleInputChange} />
        );
    }

}

export default CharactersList;