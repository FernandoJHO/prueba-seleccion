import React from 'react';
import {
    Button,
    Table,
    Container,
    Row,
    Col,
    Input
  } from 'reactstrap';
import { Link } from 'react-router-dom';
import PaginationComponent from "react-reactstrap-pagination";

function Page(props) {
    const {
        currentCharacters,
        handleSelected,
        handleInputChange,
        totalCharacters,
        charactersPerPage
    } = props; 

    const characters = currentCharacters.map(character => 
            <tr key={character.id}>
                <td>{character.name}</td>
                <td>{character.gender}</td>
                <td>{character.house}</td>
                <td><Link to={`/character/${character.id}`}><Button color="info" outline>Ver</Button></Link></td>
            </tr>
        );

    return (
        
            <Container fluid>
                <br></br>
                <Row>
                    <Col>
                    <Input type="text" name="filterContent" id="filterContent" placeholder="Búsqueda por nombre o casa" onChange={handleInputChange}/>
                    </Col>
                </Row>
                <br></br>
                <Row>
                    <Table hover>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>House</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {characters}
                        </tbody>
                    </Table>            
                
                    <PaginationComponent totalItems={totalCharacters} pageSize={charactersPerPage} maxPaginationNumbers={5} onSelect={handleSelected} />
                </Row>
            </Container> 
        
    );
}

export default Page;