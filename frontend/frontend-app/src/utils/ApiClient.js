import axios from 'axios'

const get = (path) =>
    axios.get(path);

const ApiClient = {
    get,
}

export default ApiClient;